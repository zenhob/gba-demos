//! # GBA tilemap support
//!
//! Defines the type `BGTile` which is used to map tiles in video memory.
//!
//! ```
//! // create a tile with specified sprite index
//! BGTile::new().with_tile(12);
//! ```

use bitfield_struct::bitfield;

/// Text BG Screen (2 bytes per entry)
///
/// Specifies the tile number and attributes. Note that BG tile numbers are always specified in
/// steps of 1 (unlike OBJ tile numbers which are using steps of two in 256 color/1 palette mode).
///
/// ```text
/// Bit   Expl.
/// 0-9   Tile Number     (0-1023) (a bit less in 256 color mode, because
///                          there'd be otherwise no room for the bg map)
/// 10    Horizontal Flip (0=Normal, 1=Mirrored)
/// 11    Vertical Flip   (0=Normal, 1=Mirrored)
/// 12-15 Palette Number  (0-15)    (Not used in 256 color/1 palette mode)
/// ```
#[bitfield(u16)]
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct BGTile {
    #[bits(10)]
    pub tile: u16,
    pub hflip: bool,
    pub vflip: bool,
    #[bits(4)]
    pub palette: u16,
}
