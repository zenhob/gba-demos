//!
//! # "CGA-style" text mode for GBA
//!
//! Renders a 30x20 scrolling text display on the GBA screen,
//! with provided print and println macros.
//!
//! ```rust
//! use fug::cga;
//! cga::init();
//! cga::println!("Hello, world{}", "!");
//! ```
//!

use crate::tile::BGTile;
use ::voladdress::{Safe, VolBlock};
use gba::prelude::*;

/// The GBA contains 96 Kbytes VRAM built-in, located at address 06000000-06017FFF
const VRAM_START: usize = 0x0600_0000;

/// The tile map part of VRAM starts 4000h into VRAM
const TILEMAP_START: usize = 0x0600_4000;

/// Number of text columns
///
/// The screen is 240 pixels wide with 8 pixels per character
const COLUMNS: usize = 30;

/// Number of text rows
///
/// The screen is 160 pixels tall with 8 pixels per character
const ROWS: usize = 20;

/// Tilemap memory area of VRAM
const BGMAP_TILES: VolBlock<BGTile, Safe, Safe, 1024> = unsafe { VolBlock::new(TILEMAP_START) };

/// Intialize text display mode, clear the screen, set the palette and load the font.
pub fn init(fg: Option<Color>, bg: Option<Color>) {
    // use BG mode 0 (256x256 pixels, or 32x32 tiles)
    DISPCNT.write(
        DisplayControl::new()
            .with_display_mode(0)
            .with_display_bg0(true)
            .with_display_obj(true),
    );
    BG0CNT.write(
        BackgroundControl::new()
            .with_char_base_block(0)
            .with_screen_base_block(8),
    );

    // populate the palette
    const BLACK: Color = Color::from_rgb(0, 0, 0);
    const GRAY: Color = Color::from_rgb(16, 16, 16);
    BG_PALETTE.index(0).write(fg.unwrap_or(BLACK));
    BG_PALETTE.index(1).write(bg.unwrap_or(GRAY));
    mode3::dma3_clear_to(BLACK);

    load_font();
}

/// Write a character to the screen
pub fn write_char(c: u8, x: usize, y: usize) {
    assert!(x < COLUMNS);
    assert!(y < ROWS);
    BGMAP_TILES
        // add y * 2 so we skip 2 tiles per column,
        // since the tilemap is 32 wide (2 wider than the screen)
        .index(COLUMNS * y + x + (y * 2))
        .write(BGTile::new().with_tile(c.into()));
}

/// Load a CGA-style font into the tile image area of VRAM
fn load_font() {
    use core::mem::size_of_val;
    use gba::art::CGA_8X8_THICK;

    let info = UnpackInfo {
        source_data_len_bytes: size_of_val(&CGA_8X8_THICK) as u16,
        source_unit_bit_width: 1,
        // this assumes we want to unpack to 4bpp tiles
        destination_unit_bit_width: 4,
        data_offset: 0,
    };
    // unpack directly to the start of video ram (for tile mode)
    unsafe {
        BitUnPack(
            CGA_8X8_THICK.as_ptr().cast::<u8>(),
            VRAM_START as *mut u32,
            &info,
        )
    };
}

pub struct Writer {
    column_position: usize,
}

impl Writer {
    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= COLUMNS {
                    self.new_line();
                }
                let x = self.column_position;
                let y = ROWS - 1;
                write_char(byte, x, y);
                self.column_position += 1;
            }
        }
    }

    pub fn write_string(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                // printable ASCII byte or newline
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                // not part of printable ASCII range
                _ => self.write_byte(0xfe),
            }
        }
    }

    fn new_line(&mut self) {
        // move the existing tiles up by one row
        for (idx, tile) in BGMAP_TILES
            // since the tilemap row size is actual 32,
            // we are moving rows of 32 instead of 30
            .iter_range(32..32 * (ROWS + 1))
            .enumerate()
        {
            BGMAP_TILES.index(idx).write(tile.read());
        }
        self.column_position = 0;
    }
}

use core::fmt;

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

/// Global writer state
static WRITER: Mutex<Writer> = Mutex::new(Writer { column_position: 0 });

/// Print a string to the screen
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::cga::_print(format_args!($($arg)*)));
}

/// Print a string and newline to the screen
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    WRITER.lock().write_fmt(args).unwrap();
}
