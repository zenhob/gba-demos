#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]

use fug::*;

#[no_mangle]
fn main() -> ! {
    cga::init(None, None);

    #[cfg(test)]
    test_main();

    loop {
        wait_for_vblank();
    }
}

#[cfg(test)]
fn test_runner(tests: &[&dyn Fn()]) {
    use gba::debugging::*;
    set_debug_interface(&mgba::MGBADebugInterface);

    println!("Running tests...");

    for test in tests {
        test();
    }

    println!("Tests completed!");
    crash();
}

#[test_case]
fn trivial_test() {
    assert_eq!(1, 1);
    println!("[ok] Trivial test");
}

#[test_case]
fn noise_zero_test() {
    assert!(squirrel3::noise(0, 0) > 0, "noise(0) returned zero");
    println!("[ok] noise(0)");

    assert!(squirrel3::noise2d(0, 0, 0) > 0, "noise2d(0) returned zero");
    println!("[ok] noise2d(0)");

    assert!(
        squirrel3::noise3d(0, 0, 0, 0) > 0,
        "noise3d(0) returned zero"
    );
    println!("[ok] noise3d(0)");
}
