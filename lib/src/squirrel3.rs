//!
//! Squirrel3 noise function, produces 32 random bits given a position and seed.
//!
//! Based on the [GDC talk by Squirrel Eiserloh](https://www.youtube.com/watch?v=LWFzPP8ZbdU)
//!

#![allow(dead_code)]

/// Simple noise function, accepts a position and seed
pub fn noise(position: u32, seed: u32) -> u32 {
    let mut mangled = position.wrapping_mul(0xB5297A4D);
    mangled = mangled.wrapping_add(seed);
    mangled = mangled ^ (mangled >> 8);
    mangled = mangled.wrapping_add(0x68E31DA4);
    mangled = mangled ^ (mangled << 8);
    mangled = mangled.wrapping_mul(0x1B56C4E9);
    mangled = mangled ^ (mangled >> 8);
    mangled
}

/// Normalize noise as a float between 0.0 and 1.0
pub fn normalized(position: u32, seed: u32) -> f32 {
    noise(position, seed) as f32 / u32::MAX as f32
}

/// 2-dimensional noise function
pub fn noise2d(x: u32, y: u32, seed: u32) -> u32 {
    noise(x.wrapping_add(y.wrapping_mul(198491317)), seed)
}

/// 3-dimensional noise function
pub fn noise3d(x: u32, y: u32, z: u32, seed: u32) -> u32 {
    noise(
        x.wrapping_add(y.wrapping_mul(198491317))
            .wrapping_add(z.wrapping_mul(6542989)),
        seed,
    )
}
