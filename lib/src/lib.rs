#![no_std]
#![feature(isa_attribute)]
#![feature(custom_test_frameworks)]

//!
//! Provides tools for the GBA including:
//!
//! * CGA-style text output
//! * random noise generation
//!

pub mod cga;
pub mod squirrel3;
pub mod tile;

use gba::prelude::*;

/// Wait for a vblank interrupt.
/// This signals that the screen is finished drawing.
pub fn wait_for_vblank() {
    unsafe { VBlankIntrWait() };
}

/// Initialize the CPU interrupts
pub fn irq_init() {
    // Set the IRQ handler to use.
    unsafe { USER_IRQ_HANDLER.write(Some(irq_handler_a32)) };

    // Enable IRQs
    DISPSTAT.write(DisplayStatus::new().with_vblank_irq_enabled(true));

    // enable vblank IRQ
    let /*mut*/ flags = InterruptFlags::new().with_vblank(true);
    // we can enable other interrupt types here
    unsafe { IE.write(flags) };

    // Enable all interrupts that are set in the IE register.
    unsafe { IME.write(true) };
}

/// Interrupt handler (ARM version)
#[instruction_set(arm::a32)]
extern "C" fn irq_handler_a32() {
    // we just use this a32 function to jump over back to t32 code.
    irq_handler_t32()
}

/// Interrupt handler (thumb version)
fn irq_handler_t32() {
    // disable Interrupt Master Enable to prevent an interrupt during the handler
    unsafe { IME.write(false) };

    // read which interrupts are pending, and "filter" the selection by which are
    // supposed to be enabled.
    let which_interrupts_to_handle = IRQ_PENDING.read() & IE.read();

    // read the current IntrWait value. It sorta works like a running total, so
    // any interrupts we process we'll enable in this value, which we write back
    // at the end.
    let mut intr_wait_flags = INTR_WAIT_ACKNOWLEDGE.read();

    if which_interrupts_to_handle.vblank() {
        //vblank_handler();
        intr_wait_flags.set_vblank(true);
    }

    // acknowledge that we did stuff.
    IRQ_ACKNOWLEDGE.write(which_interrupts_to_handle);

    // write out any IntrWait changes.
    unsafe { INTR_WAIT_ACKNOWLEDGE.write(intr_wait_flags) };

    // re-enable as we go out.
    unsafe { IME.write(true) };
}

/// Display an error and halt the program
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    use gba::fatal;
    print!("{}", info);
    fatal!("{}", info);
}
