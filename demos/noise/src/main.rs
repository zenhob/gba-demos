#![no_std]
#![no_main]

//!
//! # Random Noise Demo
//!
//! Produces random noise on every tick
//!

use gba::prelude::*;

use fug::*;

const SEED: u32 = 123;

/// Main entry point and game loop
#[no_mangle]
fn main() -> ! {
    let mut tick: u32 = 0;

    cga::init(
        Some(Color::from_rgb(0, 0, 31)),
        Some(Color::from_rgb(24, 24, 24)),
    );

    irq_init();

    loop {
        println!("{:#x}", squirrel3::noise(tick, SEED));
        // read input
        //let _this_frame_keys: Keys = KEYINPUT.read().into();
        // if this_frame_keys.l() {
        //     x = x - 1;
        // }
        // if this_frame_keys.r() {
        //     x = x + 1;
        // }

        // update the screen

        wait_for_vblank();
        tick += 1;
    }
}
