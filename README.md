# Game Boy Advance Demo

## Dependencies

### Latest rust

The source is needed so we can build the Rust core for ARM.

```bash
rustup install nightly
rustup +nightly component add rust-src
```

### Build utilities

We need ARM `binutils`, `cargo-make` and `gbafix` for building GBA binaries.

```bash
sudo apt install binutils-arm-none-eabi
cargo install cargo-make gbafix
```

### Emulators

I like to have both builds of mGBA available.

```bash
sudo apt install mgba-sdl mgba-qt
```

## Run the tests

This will run the tests in an emulator:

```bash
cargo test
```

## Run a demo program

For example, run the `noise` demo:

```bash
cargo run --bin noise
```
## Build a GBA ROM

This creates a binary ROM that can run on GBA hardware:

```bash
cargo build --release
arm-none-eabi-objcopy -O binary target/thumbv4t-none-eabi/release/noise noise.gba
gbafix noise.gba
mgba noise.gba
```
