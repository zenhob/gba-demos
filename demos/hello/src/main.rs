#![no_std]
#![no_main]

//!
//! # Hello World
//!
//! Demo of basic GBA text output
//!

use fug::*;

/// Main entry point and game loop
#[no_mangle]
fn main() -> ! {
    cga::init(None, None);

    println!("Hello, world!");

    irq_init();

    loop {
        wait_for_vblank();
    }
}
